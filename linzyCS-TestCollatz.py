#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "333 359\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 333)
        self.assertEqual(j, 359)

    def test_read_3(self):
        s = "789 3510\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  789)
        self.assertEqual(j, 3510)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_3(self):
        v = collatz_eval(1000, 1001)
        self.assertEqual(v, 143)

    def test_eval_4(self):
        v = collatz_eval(1, 1001)
        self.assertEqual(v, 179)

    def test_eval_5(self):
        v = collatz_eval(1, 2)
        self.assertEqual(v, 2)

    def test_eval_6(self):
        v = collatz_eval(999999, 999999)
        self.assertEqual(v, 259)
    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 2, 35, 112)
        self.assertEqual(w.getvalue(), "2 35 112\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 737747, 380330, 509)
        self.assertEqual(w.getvalue(), "737747 380330 509\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("1 999999\n853323 1323\n787 777\n737 738\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 999999 525\n853323 1323 525\n787 777 122\n737 738 140\n")

    def test_solve_3(self):
        r = StringIO("35 47\n58 19\n151 339\n164 319\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "35 47 110\n58 19 113\n151 339 144\n164 319 131\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
